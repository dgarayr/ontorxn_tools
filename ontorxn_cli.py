import argparse
from ontorxn_tools import *

def arg_handling():
	argparser = argparse.ArgumentParser()
	g1 = argparser.add_argument_group("File management")
	g1.add_argument("--ontofile","-o",help="Directory containing the ontology file",type=str,required=True)
	g1.add_argument("--graphfile","-g",help="Route to the graph file (DOT format)",type=str,default=None)
	g1.add_argument("--reportid","-r",help="ID of the report in ioChem-BD",type=int,required=True)
	g1.add_argument("--loginfile","-l",help="Configuration file with login information",type=str,required=True)
	g1.add_argument("--cmldir","-cd",help="Directory to read pre-existing CML files from",type=str,default=None)
	g2 = argparser.add_argument_group("Control options")
	g2.add_argument("--fetchfiles","-f",help="Download CML files associated to the report in ioChem-BD",
					action="store_true")
	g2.add_argument("--collapse","-c",help="Collapse nodes with common names",
					action="store_true")
	g2.add_argument("--reasoner","-rs",help="Run default reasoner on the KG",
					action="store_true")
	g2.add_argument("--stylesheet","-s",help="Program stylesheet to be used (Gaussian/ADF)",type=str,default="Gaussian")
	try:
		args = argparser.parse_args()
	except:
		print("Some arguments were missing")
		argparser.print_help()
		return None
	return args

def main():
	args = arg_handling()
	print(args)
	if (not args):
		print("Could not generate the knowledge graph")
		return None
	### Check extension
	graph_file = args.graphfile
	if ".dot" in graph_file:
		# legacy mode
		outfile = args.graphfile.replace(".dot",".owl")
		kg_func = knowledge_graph_gen_legacy
	elif ".json" in graph_file:
		outfile = args.graphfile.replace(".json",".owl")
		kg_func = knowledge_graph_gen
	else:
		print("No valid graph file produced (only .json files are supported)")
		print("Retrieving graph from ioChem-BD")
		graph_file = None
		kg_func = knowledge_graph_gen
		outfile = "kg.owl"


	if ("OntoRXN.owl" in args.ontofile):
		args.ontofile = args.ontofile.replace("/OntoRXN.owl","")
	kg_func(ontology_route=args.ontofile,report_id=args.reportid,
						config_file=args.loginfile, graph_file=graph_file,
						out_file=outfile,collapse_graph=args.collapse,
						fetch_files=args.fetchfiles,use_reasoner=args.reasoner,
						stylesheet=args.stylesheet,
						cml_dir=args.cmldir)

if (__name__ == "__main__"):
	main()
