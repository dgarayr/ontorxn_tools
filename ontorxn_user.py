'''Diego Garay-Ruiz, February 2022.
Management of OntoRXN-compliant graphs via SPARQL querying to recover basic
reaction network structure directly from the KG'''

import ontorxn_tools as orx
import networkx as nx
from operator import itemgetter
from collections import OrderedDict,defaultdict
import re

class QueryCore:
	'''Basic class to handle SPARQL query generation at the Python level'''
	def __init__(self,select=None,where=[],prefix={},after=[]):
		# Definition of basic attributes: SELECT (string) and WHERE (list of strings) statement information,
		# namespace prefixes (dict mapping prefix to URI) and possible additional clauses after selection (list)
		self.Select = select
		self.Where = where
		self.Prefix = prefix
		self.After = after
		# Definition of basic namespaces for OntoRXN and Gainesville Core
		self.QueryNamespaces = {"rxn":"http://www.semanticweb.com/OntoRxn#",
		    					"gc":"http://purl.org/gc/"}
	def set_prefix_statement(self):
		prefix_block = ""
		for key,ns in self.Prefix.items():
			ns_string = "PREFIX %s: <%s>\n" % (key,ns)
			prefix_block += ns_string
		self.PrefixString = prefix_block
		return prefix_block

	def set_where_statement(self):
		where_statement = " .\n".join(self.Where)
		where_block = "WHERE {\n%s\n}\n" % where_statement
		self.WhereString = where_block
		return where_block

	def set_select_statement(self):
		where_block = self.set_where_statement()
		select_block = "SELECT %s %s" % (self.Select,self.WhereString)
		self.SelectString = select_block
		return select_block

	def set_after_statement(self):
		after_block = "\n".join(self.After)
		self.AfterString = after_block
		return after_block
	
	def string_builder(self):
		# Build a string for the SPARQL query from defined attributes. 
		sparql_string = ""
		if (self.Prefix):
			sparql_string += self.set_prefix_statement()
		# SELECT and WHERE must be present: do a sanity check before building the structure
		# which is SELECT select-clauses WHERE { where_clauses }
		if (not self.Select or not self.Where):
			return None
		sparql_string += self.set_select_statement()
		# Add additional statements if present
		if (self.After):
			sparql_string += self.set_after_statement()
		return sparql_string

	def count_args(self,arg_type=None):
		# Count the number of optional string arguments in a given string
		if (not arg_type):
			clause = str(self)
		else:
			clause = getattr(self,arg_type)
		Nargs = clause.count("%s")
		return Nargs

	def pass_args_clause(self,clause,arglist):
		# Pass necessary arguments to the clause and remove them from the input list
		Nargs = clause.count("%s")
		if (Nargs > 0):
			pass_args = arglist[:Nargs]
			clause = clause % tuple(pass_args)
			arglist[:] = arglist[Nargs:]
		return clause
	
	def pass_args(self,arg_list):
		Nargs = self.count_args()
		if (Nargs != len(arg_list)):
			print("%d args passed, required %d" % (len(arg_list),Nargs))
			return None
		# Check Select, Where and After clauses
		self.Select = self.pass_args_clause(self.Select,arg_list)
		self.Where = [self.pass_args_clause(where_clause,arg_list) for where_clause in self.Where]
		self.After = [self.pass_args_clause(after_clause,arg_list) for after_clause in self.After]
		return self

	def get_results(self,rdf_database,attributes=None,subst={}):
		'''Get the requested attributes from a query.
		Input:
		- rdf_database. RDFLib graph to do the query on.
		- attributes. List of strings with the labels to be extracted from query results, with the
		same name as the variables in SPARQL.
		- subst. Dict of tuples for regex-based substitutions on query results, key-ed by
		attribute names, with first value element being the pattern to find and the second the substitution
		'''
		qr = rdf_database.query(str(self))
		if (not attributes):
			# use all by default and assign to a property
			attributes = [item.toPython().replace("?","") for item in qr.vars]
			self.CurrentAttr = attributes
			
		result_table = []
		for row in qr:
			raw_data = [getattr(row,attr) for attr in attributes]
			# Using the .toPython() function to keep typing
			data = [entry.toPython() if entry else entry for entry in raw_data]
			clear_flag = [attr in subst.keys() for attr in attributes]
			clean_data = [re.sub(*subst[attributes[ii]],entry) if clear_flag[ii] else entry
						  for ii,entry in enumerate(data)]
			result_table.append(tuple(clean_data))
		return result_table
	
	def results_to_dict(self,result_table,attributes):
		# Groups results to a list of dicts, mapping attribute names to their values
		if (not attributes):
			attributes = self.CurrentAttr
		result_dict_table = [OrderedDict(zip(attributes,entry)) for entry in result_table]
		return result_dict_table

	def get_hierarchical_results(self,rdf_database,attributes=None,subst={}):
		result_table = self.get_results(rdf_database,attributes,subst)
		result_dict_table = self.results_to_dict(result_table,attributes)
		return result_dict_table
		
	def __str__(self):
		'''Wrapper to call self.string_builder() if the str built-in is used'''
		return self.string_builder()

class QueryManager:
	'''Wrapper class for managing SPARQL queries for OntoRXN-based knowledge graphs'''
	def __init__(self):
		# Define the namespaces, so more can be added when passing a query
		self.QueryNamespaces = {"rxn":"http://www.semanticweb.com/OntoRxn#",
								"gc":"http://purl.org/gc/"}
		# Some useful fragments to build custom queries
		self.QueryFragments = {
			"wherePropertyMap": ["?stgX rxn:hasSpecies ?spcX",
								 "?spcX rxn:hasCalculation ?calcX",
								 "?calcX gc:hasResult / %s / gc:hasValue ?Val"]
			,
			"whereGetNode": ["?stepX rxn:hasNode ?stgX"],
			"whereGetEdge": ["?stepX rxn:hasTS ?stgX"],
			"whereGetConnections": ["?stepX rxn:hasNode ?stgX",
									"OPTIONAL {?stepX rxn:hasTS ?stgY}"],
			
			"selectSum": "?stgX (SUM(?Val) as ?ValSum)",
			"selectConcat": "?stgX (GROUP_CONCAT(?Val) as ?ValSeq)",
			"selectGetConnections": "?stepX (GROUP_CONCAT(?stgX ; separator=';') as ?stgL) ?stgY"
		}
		
		# Declare the query skeletons, including the query generation function
		self.QuerySkeletons = {
			"fetchEnergy": self.build_grouped_query(group_type="sum"),
			"fetchGraph": self.build_graph_query(),
			"fetchProperty":self.build_grouped_query(group_type="concat")
		}

	def build_dataprop_query(self,sel_clause,where_clause,select_entries='all'):
		if (select_entries == "node"):
			where_clause = [self.QueryFragments["whereGetNodeEdge"][0] % "rxn:hasNode"] + where_clause
		if (select_entries == "edge"):
			where_clause = [self.QueryFragments["whereGetNodeEdge"][0] % "rxn:hasEdge"] + where_clause
		data_query = QueryCore(sel_clause,where_clause,prefix=self.QueryNamespaces,
							   after=["GROUP BY ?stgX"])
		return data_query

	def build_grouped_query(self,group_type="sum",select_entries='all'):
		if (group_type.lower() == "sum" or group_type.lower() == "concat"):
			query_key = "select" + group_type.capitalize()
			query_pattern = self.QueryFragments[query_key]
			group_query = self.build_dataprop_query(sel_clause=query_pattern,
							where_clause=self.QueryFragments["wherePropertyMap"],
							select_entries=select_entries)
			return group_query
		else:
			return None

	def build_graph_query(self):
		graph_query = QueryCore(select=self.QueryFragments["selectGetConnections"],
								where=self.QueryFragments["whereGetConnections"],
								prefix=self.QueryNamespaces,
								after=["GROUP BY ?stepX"])
		return graph_query

	def add_namespace(self,ns_entry):
		self.QueryNamespaces.update(ns_entry)
		return None

	def set_query(self,query_type,arg_list=[],custom_query=None):
		'''Prepare a SPARQL query, either from the template dictionary or passing
		a custom QueryCore object'''
		
		if (not custom_query):
			self.QueryType = query_type
			self.CurrentSkeleton = self.QuerySkeletons.get(self.QueryType)
		else:
			self.CurrentSkeleton = custom_query
			
		if (not self.CurrentSkeleton):
			self.CurrentQuery = None
			return False
		
		Nargs = self.CurrentSkeleton.count_args()
		valid_args = (len(arg_list) == Nargs)
		if (not valid_args):
			self.CurrentQuery = None
			return False

		final_query = self.CurrentSkeleton.pass_args(arg_list)
		self.CurrentQuery = final_query
		return True

	def produce_query(self,query_type,arg_list=[],custom_query=None):
		query_flag = self.set_query(query_type,arg_list,custom_query)
		if (query_flag):
			return self.CurrentQuery
		else:
			return None


### Functions for common operations to manage KGs (get energies, connectivity...)
### 
def item_renamer(namespace,prefix,item):
	return item.replace(namespace,prefix)

def KG_energy_lookup(onto_manager,energy_property,ns_name="",custom_query=None):
	'''Use queries to sum energies on nodes and edges across all NetworkStage items in
	the knowledge graph.
	Input:
	- onto_manager. Instantiated ontorxn_tools.OntoRXNWrapper with a loaded knowledge graph
	- energy_property. String, name of the energy type to be fetched.
	- ns_name. String, name of the namespace to be summarized in the KG items.
	- custom_query. Custom QueryCore object to override the default querying behavior.
	Output:
	- energy_dict. Dictionary mapping NetworkStage names (from default identifier) to the Gibbs energies
	from the CompCalculation objects for the ChemSpecies in the stage'''
	E_query = QueryManager()
	if (not custom_query):
		if ("rxn" not in energy_property):
			energy_property = "rxn:" + energy_property
		q_energy = E_query.produce_query("fetchEnergy",[energy_property])
	else:
		q_energy = custom_query

	qr_energy = onto_manager.MainWorld.query(str(q_energy))
	energy_dict = {item_renamer(ns_name,"",row.stgX):float(row.ValSum) for row in qr_energy}
	return energy_dict

def KG_connectivity_lookup(onto_manager,ns_name="",custom_query=None):
	'''Use queries to check the connectivity between NetworkStage items across the knowledge graph.
	Input:
	- onto_manager. Instantiated ontorxn_tools.OntoRXNWrapper with a loaded knowledge graph
	- ns_name. String, name of the namespace to be summarized in the KG items.
	- custom_query. Custom QueryCore object to override the default querying behavior.
	Output:
	- connectivity. List of tuples, containing the pairs of connected nodes (as NetworkStage labels)
	and possibly, the corresponding TS if present (as last element, else None)'''
	query = QueryManager()
	if (not custom_query):
		q_connect = query.produce_query("fetchGraph")
	else:
		q_connect = custom_query
	qr_connect = onto_manager.MainWorld.query(str(q_connect))
	connect_table = [str(row.stgL).split(";") + [str(row.stgY)] for row in qr_connect]
	# Transform to tuples and rename if necessary
	connectivity = [tuple([item_renamer(ns_name,"",item) if item else None for item in entry])
					for entry in connect_table]
	return connectivity

def KG_define_network(onto_manager,ns_name="",connectivity=[],energy_dict={},energy_type="hasElecEnergy"):
	'''Defines the basic network graph between the NetworkStage entities at the knowledge
	graph.
	Input:
	- onto_manager. Instantiated ontorxn_tools.OntoRXNWrapper with a loaded knowledge graph
	- ns_name. String, name of the namespace to be summarized in the KG items.
	- energy_dict. Dictionary mapping NetworkStage names (from default identifier) to the Gibbs energies
	from the CompCalculation objects for the ChemSpecies in the stage, as produced by KG_energy_lookup().
	If empty, the lookup is done inside the function.
	- connectivity. List of tuples, containing the pairs of connected nodes (as NetworkStage labels)
	and possibly, the corresponding TS if present (as last element, else None) as produced by
	KG_connectivity_lookup(). If empty, the lookup is done inside the function
	- energy_type. String, name of the E property to be fetched if lookup is done in the function
	Output:
	- G. NetworkX.Graph containing the stage-based graph structure and the corrresponding absolute energies.
	'''
	if (not connectivity or not energy_dict):
		energy_dict = KG_energy_lookup(onto_manager,energy_type,ns_name)
		connectivity = KG_connectivity_lookup(onto_manager,ns_name)
	# Define the graph and add connectivity and information!
	G = nx.Graph()
	for entry in connectivity:
		nodes = entry[0:2]
		node_bunch = [(nd,{"abs_energy":energy_dict[nd]}) for nd in nodes if nd not in G.nodes()]
		G.add_nodes_from(node_bunch)
		
		edge = entry[2]
		ed_energy = energy_dict.get(edge)
		edge_bunch = (nodes[0],nodes[1],{"name":edge,"abs_energy":ed_energy})
		G.add_edges_from([edge_bunch])
	return G

def basic_network_formatter(G,ref_node_name=None):
	'''Computes relative energies (in kcal/mol) and adds definitions and energies to
	missing edges based on the nodes they connect across the stage-based network obtained via
	KG_define_network().
	Input:
	- G. NetworkX.Graph containing the stage-based graph structure and the corrresponding absolute energies.
	- ref_node_name. String, name of the stage used as energy reference. If empty, the minimum-energy node
	will be selected
	Output:
	- None, modifies G in-place filling blanks and adding the "energy" property'''
	hartree_to_kcal = 627.509
	try:
		ref_node = (ref_node_name,G.nodes[ref_node_name]["abs_energy"])
	except:
		all_node_energies = list(G.nodes(data="abs_energy"))
		ref_node = min(all_node_energies,key=itemgetter(1))
	# more robust than just if/else, as it handles situations where the name is wrong or not present
	for nd in G.nodes(data=True):
		nd[1]["energy"] = (nd[1]["abs_energy"] - ref_node[1])*hartree_to_kcal

	for ed in G.edges(data=True):
		if (not ed[2]["name"]):
			ed[2]["name"] = "_".join(ed[0:2])
		if (not ed[2]["abs_energy"]):
			e_nodes = [G.nodes[nd]["energy"] for nd in ed[0:2]]
			ed[2]["energy"] = max(e_nodes)
		else:
			ed[2]["energy"] = (ed[2]["abs_energy"] - ref_node[1])*hartree_to_kcal

	# Adding layout (gTOFfee compatibility)
	G.graph["MainLayout"] = nx.spring_layout(G)
	return G
		
def network_renamer(G,old_string,new_string):
	'''Applies a batch renaming to all nodes (and edges) in the graph via
	string replacement'''
	current_node_names = list(G.nodes)
	current_edge_names = [ed[2]["name"] for ed in G.edges(data=True)]
	subst_nodes = [nd.replace(old_string,new_string) for nd in current_node_names]
	renaming_rule = {nd_or:nd_nw for nd_or,nd_nw in zip(current_node_names,subst_nodes)}
	nx.relabel_nodes(G,mapping=renaming_rule,copy=False)
	# Apply to edges too
	for ed in G.edges(data=True):
		ed[2]["name"] = ed[2]["name"].replace(old_string,new_string)
	return None
